import sys
sys.path.append("../voice_recognition") if "../voice_recognition" not in sys.path else None
sys.path.append("../") if "../" not in sys.path else None

from voice_recognition import command
from voice_recognition import interpreter
from voice_recognition import voice

import os.path
import filecmp
import unittest


class TestIntegration(unittest.TestCase):

    testDir = os.path.dirname(os.path.abspath(__file__))
    input = os.path.join(testDir, "input.txt")
    inputWave = os.path.join(testDir, "input_audio.wav")
    output = os.path.join(testDir, "output.txt")
    fakefiles = [os.path.join("./test", "cv_antonio.txt"), os.path.join("./test", "cv_marco.txt")]
    expected = os.path.join(testDir, "expected.txt")

    @classmethod
    def setUpClass(cls):

        with open(cls.input, "w") as input:
            input.write("find cv")

        for file in cls.fakefiles:
            open(file, "w")

        cmd = command.SaySoomething(**{"fromFile": cls.input, "toFile": cls.inputWave})
        cmd.do()

        with open(cls.expected, "w") as exp:
            exp.write("file(s) found:\n%s" % '\n'.join(cls.fakefiles) )


    @classmethod
    def tearDownClass(cls):
        filesToDelete = [cls.input, cls.inputWave, cls.output, cls.expected]
        filesToDelete.extend(cls.fakefiles)
        for file in filesToDelete:
            os.remove(file)


    def test_1_ready(self):
        self.assertTrue(os.path.exists(self.input))
        self.assertTrue(os.path.exists(self.inputWave))
        for fake in self.fakefiles:
            self.assertTrue(fake)
        self.assertTrue(os.path.exists(self.expected))


    def test_2_produce(self):
        micro = voice.VoiceRecorder()
        order = micro.capture(self.inputWave)

        instruction = order.split()
        self.assertEqual(instruction, ["find", "CV"])

        cmd = interpreter.Interpreter.getCommand(instruction)
        self.assertEqual(command.FindFile, type(cmd))

        cmd._kwargs["toFile"] = self.output
        cmd.do()
        self.assertTrue(os.path.isfile(self.output))

        self.assertTrue(filecmp.cmp(self.expected, self.output))


if __name__ == '__main__':
    unittest.TestLoader.sortTestMethodsUsing = lambda _, x, y: cmp(x, y)
    unittest.main()
