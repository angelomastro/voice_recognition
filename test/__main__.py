import os, os.path
import unittest

if __name__ == '__main__':

    testDir = os.path.dirname(os.path.abspath(__file__))

    testModules = list()
    for file in os.listdir(testDir):
        if len(file) > 7 and file[:4] == "test" and file[len(file)-3:] == ".py":
            testModules.append(file.split(".")[0])

    suites = list()
    for testFile in testModules:
        suites.append(unittest.defaultTestLoader.loadTestsFromName(testFile))

    test_suite = unittest.TestSuite(suites)
    test_runner = unittest.TextTestRunner().run(test_suite)