# Voice recognition #


## Mac OS ##

### Pre-requisites ###

1. Mac OS (for now)

2. Python 2.7

3. [sudo] pip install SpeechRecognition


### Using  the program ###

Use your voice to search for a file in the current directory and see the results printed on the screen

        $ python voice_recognition


### (Integration) Test ###

This test creates a voice record "find cv" by our beloved Tessa, and executes the voice_recognition from it.
It succeeds when it prints out the right files to be found.

        $ python test


### Notes: using voice by Mac ###

a. save a file with your speach

        $ echo "hello south africa!" > input.txt

        # FYI: if you have a Word file you can convert it with
        $ textutil -convert txt <input.docx> <input.txt>

b. read it with a given voice (I like south-african accent)

        $ say -v Tessa -f input.txt

        # ... or read hardcoded text
        $ say -v Tessa "hello world"

        # ... or save a file (txt) into an audio (wav) file
        $ say -v Tessa -f input.txt -o audio.aiff --file-format=AIFF --data-format=BEF32@8000

c. save an audio file and replay

        # by default mac saves file as .aiff, but you can change that with --file-format=mp4f option
        $ say -v Tessa -f input.txt -o output

        # Tessa will now read the file. Cool, right?
        $ afplay output.aiff
