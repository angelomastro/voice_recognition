#Python 2.x
import speech_recognition as sr


class VoiceRecorder(object):

    def __init__(self, **kwargs):
        self.sampleRate = int(kwargs.get("sampleRate", "48000")) # how often values are recorded
        self.chunkSize = int(kwargs.get("chunkSize", "2048")) # buffer size (bytes): better use powers of 2
        self.micName = kwargs.get("micName", "Built-in Output")
        self.rec = sr.Recognizer()
        self.deviceId = self.__getMicId(self.micName, sr.Microphone.list_microphone_names())


    def capture(self, audioFile=None):
        if audioFile:
            return self.__captureFromFile(audioFile)
        return self.__capture()


    def __getMicId(self, micName, micList):
        for i, name in enumerate(micList):
            if name == micName:
                return i-1
        return 1 #default mic, else -> #raise Exception("could not find microphone id for %s " % micName)

    def __capture(self):

        try:
            with sr.Microphone(device_index = self.deviceId,
                               sample_rate = self.sampleRate,
                               chunk_size = self.chunkSize) as source:

                self.rec.adjust_for_ambient_noise(source)

                audio = self.rec.listen(source)

                try:
                    text = self.rec.recognize_google(audio)
                    return str(text)


                except sr.UnknownValueError:
                    print("Google Speech Recognition could not understand audio")

                except sr.RequestError as e:
                    print("Could not request results from Google Speech Recognition service; {0}".format(e))

        except Exception as e:
            print("Error: {0}".format(e))

        return ""



    def __captureFromFile(self, audioFile):

        try:
            with sr.WavFile(audioFile) as source:

                audio = self.rec.record(source)

                try:
                    text = self.rec.recognize_google(audio)
                    return str(text)

                except sr.UnknownValueError:
                    print("Google Speech Recognition could not understand audio")

                except sr.RequestError as e:
                    print("Could not request results from Google Speech Recognition service; {0}".format(e))

        except Exception as e:
            print("Error: {0}".format(e))

        return ""


