import os.path
import subprocess as sp
from sys import stderr


class Command(object):

    def __init__(self, **kwargs):
        self._kwargs = kwargs

    def do(self):
        pass

    def printResults(self, results, output=""):
        if output:
            with open(output, "wb") as logbuf:
                logbuf.write(results)
        else:
            stderr.write(results + "\n")


class NullCommand(Command):

    def do(self):
        return 0


class FindFile(Command):

    def do(self):

        fileName = self._kwargs["file"]
        dirName = self._kwargs.get("dir", ".")
        toFile = self._kwargs.get("toFile", "")

        args = ["find", dirName, "-name", fileName]
        p1 = sp.Popen(args, stdout=sp.PIPE)
        p1.wait()
        ret = p1.communicate()[0]

        if len(ret):
            retList = ret.split("\n")
            retList.pop(len(retList)-1)
            allfiles = ["file(s) found:"]
            allfiles.extend(retList)
            self.printResults("\n".join(allfiles), toFile)
        else:
            self.printResults("file '%s' not found\n" % fileName, toFile)

        return p1.returncode


class SaySoomething(Command):

    voices = {"en_ZA": "Tessa"} #todo: add more languages, adjust the commands accordingly

    def do(self):

        lang = self._kwargs.get("lang", "en_ZA")
        voice = self.voices[lang]
        phrase = self._kwargs.get("phrase", "")
        fromFile = self._kwargs.get("fromFile", "")
        toFile = self._kwargs.get("toFile", "") #it must be a WAV file

        args = ["say", "-v", voice]

        if phrase:
            args.extend([phrase])
        elif not phrase and fromFile:
            args.extend(["-f", fromFile])

        if toFile:
            args.extend(["-o", toFile, "--data-format=LEI32@8000"])

        p1 = sp.Popen(args)
        p1.wait()
        return p1.returncode

