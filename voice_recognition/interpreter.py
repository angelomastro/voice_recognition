import command


class Interpreter(object):

    @staticmethod
    def getCommand(instructions):

        if len(instructions) == 0:
            return command.NullCommand()

        if len(instructions) > 1 and instructions[0] == "search" or instructions[0] == "find":
            kwargs = {"file": "*" + instructions[1].lower() + "*"}
            if len(instructions) > 3 and instructions[3] == "in":
                kwargs["dir"] = instructions[3]

            return command.FindFile(**kwargs)

        #todo: add new commands

        return command.NullCommand()



