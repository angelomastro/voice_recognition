import command
import interpreter
import voice

import sys


if __name__ == "__main__":

    args = sys.argv
    if len(args) > 1 and (args[1] == "-h" or args[1] == "--help"):
        print "python voice_recognition.py [ -l <language> = EN] [ -t <timeout (sec)> = 5]"
        sys.exit(0)

    micro = voice.VoiceRecorder(sampleRate=48000, chunkSize=2048)

    welcome = command.SaySoomething(phrase="What can I do for you?")
    welcome.do()

    instructions = micro.capture() #speak: "find marco ... find cv ... find antonio ... etc "

    cmd = interpreter.Interpreter.getCommand(instructions.split())
    cmd.do()

